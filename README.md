# cloud_configs

```shell
# Oh My Zsh, ZSH_THEME="robbyrussell"
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
ln -s ~/Workspace/chulin/cloud-configs/zsh/.zshrc ~/.zshrc

# Tmux
brew install tmux
ln -s ~/Workspace/chulin/cloud-configs/tmux/tmux.conf ~/.tmux.conf

# Alacritty
brew install alacritty
ln -s ~/Workspace/chulin/cloud-configs/alacritty/alacritty.yml ~/.alacritty.yml

# assh
brew install assh
ln -s ~/Workspace/chulin/cloud-configs/assh/assh.yml ~/.ssh/assh.yml
ssh-copy-id -i ~/.ssh/id_rsa.pub root@124.70.156.175
ssh-copy-id -i ~/.ssh/id_rsa.pub root@47.102.199.219
ssh-copy-id -i ~/.ssh/id_rsa.pub root@47.103.52.11
ssh-copy-id -i ~/.ssh/id_rsa.pub work@10.6.215.35
ssh-copy-id -i ~/.ssh/id_rsa.pub -p 10022 root@10.245.10.94
ssh-copy-id -i ~/.ssh/id_rsa.pub -p 10022 root@10.245.10.95
# 强制生成ssh config文件，可能需要多执行几次才能生效
assh config build > ~/.ssh/config

# git
ln -s ~/Workspace/chulin/cloud-configs/git/.gitconfig ~/.gitconfig

# kitty
ln -s ~/Workspace/chulin/cloud-configs/kitty ~/.config/kitty

```
